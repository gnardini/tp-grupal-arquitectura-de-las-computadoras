#ifndef _kasm_
#define _kasm_

#include "../../defs.h"
#include "./kdefs.h"

unsigned int    _read_msw();

void            _lidt (IDTR *idtr);

void			_Cli(void);
void			_Sti(void);	

byte  			_in(dword dir);
void  			_out(dword dir, byte data);
void 			_exce00();
void 			_exce01();
void 			_exce02();
void 			_exce03();
void 			_exce04();
void 			_exce05();
void 			_exce06();
void 			_exce07();
void			_unknown_hand();
void			_divide_zero_hand();
void			_overflow_hand();
void			_general_protection_hand();
void			_timer_tick_hand();
void			_keyboard_hand();   
dword			_syscall_hand();  
//byte			_biosFirstKeyboardFlag();

#endif