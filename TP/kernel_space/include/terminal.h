#ifndef _terminal_
#define _terminal_

#include "../../defs.h"

int stdBufferIsEmpty();

int clearStdBuffer();

void addToStdBuffer(char c);

int getFromStdBuffer(char* str, dword size);

char getCharFromBuffer();

char nextChar();

void terminalPrintn(char* str, int n);

void terminalPrint(char *str);

void terminalClearScreen();

void terminalPutHexa();

#endif