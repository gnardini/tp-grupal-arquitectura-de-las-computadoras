#ifndef _interruptions_
#define _interruptions_

#include "./kdefs.h"
#include "../../defs.h"

void loadIDTR();

void initInterruptions();

dword nextIdtEntry();

void setup_IDT_entry (int pos, byte selector, dword offset, byte access, byte cero);

/*INTERRUPTIONS*/
void setFrequencies();
void divideZeroRoutine();
void overflowRoutine();
void generalProtectionRoutine();
void unknownRoutine();
void timerTickRoutine();
void keyboardRoutine(word b);
dword syscallRoutine(dword eax, dword ebx, dword ecx, dword edx, dword esi, dword edi);
void setSoundForException(int exception, word frequency, int duration);

#endif