#ifndef _kernel_
#define _kernel_

#include "../../defs.h"
#include "./kasm.h"
#include "./terminal.h"
#include "./interruptions.h"
#include "../drivers/include/pic_driver.h"

void initKernel();

void loadIDT();

#endif