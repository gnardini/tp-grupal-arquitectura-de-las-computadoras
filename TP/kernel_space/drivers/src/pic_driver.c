#include "../include/pic_driver.h"
#include "../../include/kasm.h"

#define INIT 0x10

word pic1=0x20, pic2=0xA0;

void setPicMasks(byte m1, byte m2){
	_out(m1,pic1+1);
	_out(m2,pic2+1);
}

// stanislavs.org/helppc/8259.html
// wiki.osdev.org/PIC#Initialisation

void changePicOffsets(word off1, word off2){
	byte m1, m2;
	m1=_in(pic1+1);     // Guardo las mascaras
	m2=_in(pic2+1);
	
	_out(pic1,INIT); 	// Initialization Command Word 1
	_out(pic1+1,off1);	// Initialization Command Word 2
	_out(pic1+1,0x04);	// Initialization Command Word 3 (donde esta el slave)
						// Initialization Command Word 4 (not needed)
	_out(pic2,INIT); 	// Initialization Command Word 1
	_out(pic2+1,off2);	// Initialization Command Word 2
	_out(pic2+1,0x02);	// Initialization Command Word 3 (dice al slave )

	_out(pic1+1,m1);
	_out(pic2+1,m2);
}