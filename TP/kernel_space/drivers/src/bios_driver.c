#include "../include/bios_driver.h"

// http://wiki.osdev.org/System_Management_BIOS

void bios_routine(modelT *model) {
  byte *memPos = (byte *) SM_LOWER_BOUND; /*Punto de entrada para buscar el inicio de la System Managment Bios*/
  byte *address;

  while ((unsigned int) memPos < SM_UPPER_BOUND) {
    if (memPos[0] == '_' && memPos[1] == 'S' && memPos[2] == 'M' && memPos[3] == '_') {
        byte checksum = 0;
        dword checksumSize = memPos[5];
        int i;
        for(i = 0; i < checksumSize; i++) /*Asegura que los caracteres a leer son los datos del System Managment a traves de una convencion*/
            checksum += memPos[i];
        if(checksum == 0) /*Sale del ciclo*/
          break;
    }
    memPos += 16;
  }

    address = (byte*)*((dword*)(memPos+TABLEADDRESS));			//Direccion de la tabla
   	address += *(address+DATALENGTH);						            //stamaño de la seccion de datos
    address = mod_strcpy((byte**)&(model->marca), address);
    address = mod_strcpy((byte**)&(model->version), address);
    address = mod_strcpy((byte**)&(model->date), address);
}

byte *mod_strcpy(byte **str1, byte *str2) {
  *str1=str2;
	while(*str2 != 0) {
   	  str2++;
    }
  return str2+1;
}