#include "../include/sound_driver.h"
#include "../../include/kc.h"
#include "../../include/kasm.h"

// http://wiki.osdev.org/PC_Speaker

void makeBeep(word frequency, int time){
	startBeep(frequency);
	wait(time);
	finishBeep();
}

void startBeep(word frequency){
	dword realFrequence = 1193180;
	byte pulse;
	realFrequence/=frequency;
	_out(0x43,0xB6);
	_out(0x42, (byte) realFrequence);
	_out(0x42, (byte) (realFrequence>>8));

	pulse = _in(0x61);
	_out(0x61,pulse|3);
}

void finishBeep(){
	byte pulse = _in(0x61);
	_out(0x61,pulse&0xFC);
}
/*
 //Play sound using built in speaker
 static void play_sound(u32int nFrequence) {
 	u32int Div;
 	u8int tmp;
 
        //Set the PIT to the desired frequency
 	Div = 1193180 / nFrequence;
 	outb(0x43, 0xb6);
 	outb(0x42, (u8int) (Div) );
 	outb(0x42, (u8int) (Div >> 8));
 
        //And play the sound using the PC speaker
 	tmp = inb(0x61);
  	if (tmp != (tmp | 3)) {
 		outb(0x61, tmp | 3);
 	}
 }
 
 //make it shutup
 static void nosound() {
 	u8int tmp = (inb(0x61) & 0xFC);
 
 	outb(0x61, tmp);
 }
 
 //Make a beep
 void beep() {
 	 play_sound(1000);
 	 timer_wait(10);
 	 nosound();
          //set_PIT_2(old_frequency);
 }*/