#include "../include/pit_driver.h"
#include "../../include/kasm.h"

int counter=0;

void waitx55ms(int amount){
	counter=0;
	_Sti();
	while(counter<amount);
	_Cli();
}

void pitAction(){
	counter++;
}