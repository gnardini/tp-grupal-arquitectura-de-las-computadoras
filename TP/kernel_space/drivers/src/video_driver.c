#include "../include/video_driver.h"

#define WHITE_TXT 0x07


byte *video = (byte *) 0xB8000;

int currentPos=0;

void newLine(){
	int i;
	for(i=0; i<(WIDTH*(HEIGHT-1)*2); i++){
		video[i]=video[i+WIDTH*2];
	}
	for(i=WIDTH*(HEIGHT-1)*2; i<WIDTH*HEIGHT*2;i++)
		video[i++]=' ';
		video[i]=WHITE_TXT;
}

void videoPutChar(char c) {
	switch(c) {
		case '\n':
			currentPos += (WIDTH*2 - currentPos % (WIDTH*2));
			break;	
		case '\b':
			if(currentPos) currentPos-=2;
			video[currentPos]=' ';
			break;
		default:
			video[currentPos]=c;
			currentPos+=2;
	}
	if(currentPos>=WIDTH*HEIGHT*2){
		newLine();
		currentPos=WIDTH*(HEIGHT-1)*2;
	}
}

void videoClearScreen() {
	unsigned int i = 0;
	while(i < (WIDTH*HEIGHT*2))
	{
		video[i++]=' ';
		video[i++]=WHITE_TXT;
	}
	currentPos=0;
}
