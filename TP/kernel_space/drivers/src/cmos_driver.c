#include "../include/cmos_driver.h"

// wiki.osdev.org/CMOS

void cmosGetDate(dateT *date) {
	date->day	= fetchData(DAY_REGISTER);
	date->month = fetchData(MONTH_REGISTER);
	date->year	= fetchData(YEAR_REGISTER);
	
	if(!(fetchData(CMOS_REGISTER) & 0x04)) {
		fromBCDtoBinaryDate(date);
	}
}

void cmosGetTime(timeT *curr_time) {
	curr_time->second = fetchData(SECOND_REGISTER);
	curr_time->minute = fetchData(MINUTE_REGISTER);
	curr_time->hour	 = fetchData(HOUR_REGISTER);
	
	if(!(fetchData(CMOS_REGISTER) & 0x04)) {
		fromBCDtoBinaryTime(curr_time);
	}
}

byte fetchData(int reg) {
	_out(CMOS_OUT, reg);
	return _in(CMOS_IN);
}

void fromBCDtoBinaryDate(dateT *date) {
	date->day   = (date->day & 0x0F) + ((date->day / 16) * 10);
	date->month = (date->month & 0x0F) + ((date->month / 16) * 10);
	date->year  = (date->year & 0x0F) + ((date->year / 16) * 10);
}

void fromBCDtoBinaryTime(timeT *curr_time) {
	curr_time->second   = (curr_time->second & 0x0F) + ((curr_time->second / 16) * 10);
	curr_time->minute = (curr_time->minute & 0x0F) + ((curr_time->minute / 16) * 10);
	curr_time->hour = (curr_time->hour & 0x0F) + ((curr_time->hour / 16) * 10);
}