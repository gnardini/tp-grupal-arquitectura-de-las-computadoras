#ifndef _sound_driver_
#define _sound_driver_
#include "../../../defs.h"

void makeBeep(word frequency, int time);
void startBeep(word frequency);
void finishBeep();

#endif