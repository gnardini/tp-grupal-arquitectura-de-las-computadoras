#ifndef _cmos_driver_
#define _cmos_driver_

#include "../../include/kasm.h"
#include "../../../defs.h"

//REGISTERS
#define SECOND_REGISTER 0x00
#define MINUTE_REGISTER	0x02
#define HOUR_REGISTER	0x04
#define DAY_REGISTER	0x07
#define MONTH_REGISTER	0x08
#define YEAR_REGISTER	0x09
#define CMOS_REGISTER	0x0B

//PORTS
#define CMOS_OUT	0x70
#define CMOS_IN		0x71

void cmosGetDate(dateT *date);
void cmosGetTime(timeT *curr_time);
byte fetchData(int reg);

void fromBCDtoBinaryDate(dateT *date);
void fromBCDtoBinaryTime(timeT *curr_time);
#endif