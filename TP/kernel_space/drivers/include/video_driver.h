#ifndef _video_driver_
#define _video_driver_

#include "../../../defs.h"

#define WIDTH 80
#define HEIGHT 25
#define DEF_INT_SIZE 11

void videoPutChar(char c);
void videoClearScreen();
void initScreen();
void videoPrint(char* str);
void videoPrintn(char* str, int size);
#endif