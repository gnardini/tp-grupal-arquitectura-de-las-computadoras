#ifndef _bios_driver_
#define _bios_driver_

#include "../../../defs.h"
#include "../../include/kdefs.h"

#define SM_LOWER_BOUND 0xF0000
#define SM_UPPER_BOUND 0x100000
#define TABLEADDRESS 24
#define DATALENGTH 1

void bios_routine(modelT *model);

byte* mod_strcpy(byte **str1, byte *str2);

#endif