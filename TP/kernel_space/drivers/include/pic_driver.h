#ifndef _pic_driver_
#define _pic_driver_

#include "../../../defs.h"

void setPicMasks(byte m1, byte m2);
void changePicOffsets(word off1, word off2);

#endif