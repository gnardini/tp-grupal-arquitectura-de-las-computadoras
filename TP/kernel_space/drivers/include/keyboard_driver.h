#ifndef _keyboard_driver_
#define _keyboard_driver_
#include "../../../defs.h"

#define NOT_PRINTABLE 0
#define MAYUS 58
#define LEFT_SHIFT 42
#define RIGHT_SHIFT 54
#define CAPSLOCK 58
#define NUMLOCK 69
#define BACKSPACE 14
#define RELEASE 0x80

int keyboardPress(byte b);

#endif