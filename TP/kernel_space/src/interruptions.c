#include "../include/kasm.h"
#include "../include/kc.h"
#include "../include/interruptions.h"
#include "../include/terminal.h"
#include "../drivers/include/keyboard_driver.h"
#include "../drivers/include/pic_driver.h"
#include "../drivers/include/sound_driver.h"
#include "../drivers/include/pit_driver.h"
#include "../drivers/include/bios_driver.h"
#include "../drivers/include/cmos_driver.h"

#define IDT_SIZE 0x8F

int idtEntry=0, i=0;
word frequencies[32];
int durations[32];

DESCR_INT idt[IDT_SIZE];			/* IDT de 8F entradas*/
IDTR idtr;					/* IDTR */
byte idtEntries[IDT_SIZE]={0};

void loadIDTR() {
	idtr.base = 0;  
	idtr.base +=(dword) &idt;
	idtr.limit = sizeof(idt)-1;	
	_lidt (&idtr);
}

 /* Inicializa la entrada del IDT */
void setup_IDT_entry (int pos, byte selector, dword offset, byte access, byte cero) {
	idt[pos].selector = selector; 
	idt[pos].offset_l = offset & 0xFFFF; 	//Asigno la parte menos significativa
  	idt[pos].offset_h = offset >> 16;	//Asigno la parte mas significativa
  	idt[pos].access = access;
  	idt[pos].cero = cero;
  	idtEntries[pos]=1;
}

dword nextIdtEntry(){
	dword offset;
	while(!idtEntries[idtEntry] && idtEntry<IDT_SIZE)
		idtEntry++;
	if(idtEntry>=IDT_SIZE){
		idtEntry=0;
		return 0;
	}
	terminalPrint("Entrada: ");
	terminalPutHexa(idtEntry);
	terminalPrint("  Selector: ");
	terminalPutHexa(idt[idtEntry].selector);
	terminalPrint("  Offset: ");
	offset=(idt[idtEntry].offset_h<<16)+idt[idtEntry].offset_l;
	terminalPutHexa(offset);
	terminalPrint(" Acceso: ");
	terminalPutHexa(idt[idtEntry++].access);
	return 1;
}

void setFrequencies(){
	int k;
	for(k=0 ; k<32 ; k++){
		frequencies[k] = 500;
		durations[k] = 500;
	}
	frequencies[0] = 300;
	frequencies[4] = 600;
	frequencies[13] = 900;
}

void divideZeroRoutine(){
	terminalPrint("Error: Division por cero.\n");
	makeBeep(frequencies[0],durations[0]);
}

void overflowRoutine(){
	terminalPrint("Error: Overflow.\n");
	makeBeep(frequencies[4],durations[4]);
}

void generalProtectionRoutine(){
	terminalPrint("Error: Proteccion general.\n");
	makeBeep(frequencies[13],durations[13]);
}

void unknownRoutine(int numError){
	terminalPrint("Error ");
	terminalPutHexa(numError);
	terminalPrint(": error desconocido");
	makeBeep(frequencies[numError],durations[numError]);
}

void timerTickRoutine() {
	pitAction();
}

void keyboardRoutine(word b) {
	//int i=0;

	char c=keyboardPress(b);
	if(c){
		/*int y=10;
		while(y++<250){
		makeBeep(4*y,500000);
		if(i++%50==0) y*=0.6;
		}*/
		addToStdBuffer(c);
	}	
}

dword syscallRoutine(dword eax, dword ebx, dword ecx, dword edx, dword esi, dword edi){
	modelT bios_model;
	switch(eax){
		case 3:
			if(ebx==STD_INPUT_DESCRIPTOR){
				return getFromStdBuffer((char*)ecx,edx);
			}
			break;
		case 4:
			if(ebx==STD_OUTPUT_DESCRIPTOR)
				terminalPrintn((char*)ecx,edx);
			break;
		case 5:
			terminalClearScreen();
			break;
		case 6:
			return nextIdtEntry();
			break;
		case 7:
			return nextChar();
			break;
		case 8:
			bios_routine(&bios_model);
			terminalPrint("Marca: ");
			terminalPrint((char*)bios_model.marca);
			terminalPrint("\nVersion: ");
			terminalPrint((char*)bios_model.version);
			terminalPrint("\nFecha: ");
			terminalPrint((char*)bios_model.date);
			terminalPrint("\n");
			break;
		case 9:
			cmosGetDate((dateT *)ebx);
			break;
		case 10:
			cmosGetTime((timeT *)ebx);
			break;
		case 11:
			setSoundForException((int)ebx, (word)ecx, (int)edx);
			break;
		case 12:
			makeBeep((word)ebx, (int)ecx);
			break;
		case 13:
			return clearStdBuffer();
			break;
	}
	return 0;
}

void setSoundForException(int exception, word frequency, int duration){
	frequencies[exception] = frequency;
	durations[exception] = duration;
}
