#include "../include/kernel.h"

void initKernel() {
	terminalClearScreen();
	terminalPrintn("Inicializando kernel\n\n",0);
	_Cli();
	loadIDT();
	loadIDTR();
	setFrequencies();
	changePicOffsets(0x20,0x28);
	setPicMasks(0xFC,0xFC);
	_Sti();
	terminalPrintn("Inicializacion completa\n\n", 0);
}


void loadIDT() {
	int i;
	void (*exceHand[])() = {&_exce00, &_exce01, &_exce02, &_exce03, &_exce04, &_exce05, &_exce06, &_exce07};
	for(i=0;i<32;i++)
		setup_IDT_entry (i, 0x08, (dword)&exceHand[i%8], ACS_INT, 0);
	setup_IDT_entry (0x00, 0x08, (dword)&_divide_zero_hand, ACS_INT, 0); //Division por 0
	setup_IDT_entry (0x04, 0x08, (dword)&_overflow_hand, ACS_INT, 0); //Overflow
	setup_IDT_entry (0x0D, 0x08, (dword)&_general_protection_hand, ACS_INT, 0); //Errores generales
	setup_IDT_entry (0x20, 0x08, (dword)&_timer_tick_hand, ACS_INT, 0); //Timer Tick
    setup_IDT_entry (0x21, 0x08, (dword)&_keyboard_hand, ACS_INT, 0); //Keyboard
    setup_IDT_entry (0x80, 0x08, (dword)&_syscall_hand, ACS_INT, 0); //System calls
}
