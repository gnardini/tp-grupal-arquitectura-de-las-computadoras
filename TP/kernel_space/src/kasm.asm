GLOBAL  _read_msw,_lidt
GLOBAL  _divide_zero_hand, _overflow_hand, _general_protection_hand, _unknown_hand, _timer_tick_hand, _keyboard_hand, _syscall_hand
GLOBAL  _biosFirstKeyboardFlag
GLOBAL  _mascaraPIC1,_mascaraPIC2
GLOBAL  _Cli,_Sti, _in, _out
GLOBAL  _debug
GLOBAL  _exce00, _exce01, _exce02, _exce03, _exce04, _exce05, _exce06, _exce07

EXTERN  unknownRoutine
EXTERN  divideZeroRoutine
EXTERN  overflowRoutine
EXTERN  generalProtectionRoutine
EXTERN  timerTickRoutine
EXTERN  keyboardRoutine
EXTERN  syscallRoutine


SECTION .text


_divide_zero_hand:
        pushad
        call    divideZeroRoutine              ; Handler de Excepcion 1 (Divide by zero)
        popad
        jmp     volver

_overflow_hand:
        pushad
        call    overflowRoutine
        popad
        jmp     volver

_general_protection_hand:
        pushad
        call    generalProtectionRoutine        ; Handler de Excepcion 14 (General Protection)
        popad
        jmp     volver

volver:
        mov esp, ebp
        pop ebp
        sti
        retn

_exce00:
        pushad
        mov eax, 0h
        push eax
        call unknownRoutine
        pop eax
        popad
        jmp volver


_exce01:
        pushad
        mov eax, 1h
        push eax
        call unknownRoutine
        pop eax
        popad
        jmp volver


_exce02:
        pushad
        mov eax, 2h
        push eax
        call unknownRoutine
        pop eax
        popad
        jmp volver


_exce03:
        pushad
        mov eax, 3h
        push eax
        call unknownRoutine
        pop eax
        popad
        jmp volver

_exce04:
        pushad
        mov eax, 4h
        push eax
        call unknownRoutine
        pop eax
        popad
        jmp volver


_exce05:
        pushad
        mov eax, 5h
        push eax
        call unknownRoutine
        pop eax
        popad
        jmp volver


_exce06:
        pushad
        mov eax, 6h
        push eax
        call unknownRoutine
        pop eax
        popad
        jmp volver

_exce07:
        pushad
        mov eax, 7h
        push eax
        call unknownRoutine
        pop eax
        popad
        jmp volver



_timer_tick_hand:     
        push    ebp
        mov     ebp, esp                ; Handler de INT 8 (Timer tick)
        push    ds
        push    es                   
        pushad                     
        call    timerTickRoutine                
        mov     al,20h                  ; Envio de EOI generico al PIC
        out     20h,al             
        popad
        pop es
        pop ds
        mov     esp, ebp
        pop     ebp
        iret

_keyboard_hand:           
        push    ebp
        mov     ebp, esp                ; Handler de INT 9 (Keyboard)               
        push    ds
        push    es                   
        pushad  
        in      ax,60h                  ; Se consume la tecla del buffer
        push    eax
        call    keyboardRoutine
        pop     eax
        mov     al,20h                
        out     20h,al
        popad
        pop     es
        pop     ds
        mov     esp, ebp
        pop     ebp
        iret

_syscall_hand:    
        push    ebp
        mov     ebp, esp               ; Handler de INT 80 (System calls)
        push    ds
        push    es                      ; Se salvan los registros
        push    edi
        push    esi
        push    edx
        push    ecx
        push    ebx
        push    eax
        call    syscallRoutine
        pop     ebx
        pop     ebx
        pop     ecx
        pop     edx
        pop     esi
        pop     edi 
        pop     es
        pop     ds
        mov     esp, ebp
        pop     ebp
        iret

_in:
        push ebp
        mov ebp, esp
        push edx
        mov edx, [ebp+8]
        in al, dx
        pop edx
        mov esp, ebp
        pop ebp
        ret

_out:
        push ebp
        mov ebp, esp
        push eax
        push edx
        mov edx, [ebp+8]
        mov eax, [ebp+12]
        out dx, al
        pop edx
        pop eax
        mov esp, ebp
        pop ebp
        ret

_Cli:
	cli			; limpia flag de interrupciones
	ret

_Sti:

	sti			; habilita interrupciones por flag
	ret

_mascaraPIC1:			; Escribe mascara del PIC 1
	push    ebp
        mov     ebp, esp
        mov     ax, [ss:ebp+8]  ; ax = mascara de 16 bits
        out	21h,al
        pop     ebp
        retn

_mascaraPIC2:			; Escribe mascara del PIC 2
	push    ebp
        mov     ebp, esp
        mov     ax, [ss:ebp+8]  ; ax = mascara de 16 bits
        out	0A1h,al
        pop     ebp
        retn

_read_msw:
        smsw    ax		; Obtiene la Machine Status Word
        retn


_lidt:				; Carga el IDTR
        push    ebp
        mov     ebp, esp
        push    ebx
        mov     ebx, [ss: ebp + 6] ; ds:bx = puntero a IDTR 
	rol	ebx,16		    	
	lidt    [ds: ebx]          ; carga IDTR
        pop     ebx
        pop     ebp
        retn