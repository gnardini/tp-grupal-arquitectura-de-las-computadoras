#include "../include/terminal.h"
#include "../drivers/include/video_driver.h"
#include "../include/kasm.h"

#define BUFFER_SIZE 512

char buffer[BUFFER_SIZE]={0};
int toReadIndex=0,writedIndex=0,auxWriteIndex=0;
int isGetchActive=0, nextch=0;

int stdBufferIsEmpty(){
    return toReadIndex == writedIndex;
}

char nextChar(){
    nextch=0;
    isGetchActive=1;
    _Sti();
    while(!nextch);
    _Cli();
    isGetchActive=0;
    return nextch;
}

//Devuelve si el buffer estaba vacio antes de borrarlo
int clearStdBuffer(){
    int empty = writedIndex-toReadIndex;
    toReadIndex = auxWriteIndex = writedIndex;
    return !empty;
}

void addToStdBuffer(char c) {
    int canPrint=1;
    if(isGetchActive){
        nextch=c;
        return;
    }
    switch (c){
        case '\b':
            canPrint = writedIndex != auxWriteIndex;
            if(canPrint)
                auxWriteIndex--;
            break;    
        case '\n':
            buffer[auxWriteIndex++]=c;
            writedIndex=auxWriteIndex;
            break;
        default:
            canPrint=(auxWriteIndex+2)%BUFFER_SIZE!=toReadIndex;
            if(canPrint)
                buffer[auxWriteIndex++]=c;
    }
    if(canPrint)
        videoPutChar(c);
    auxWriteIndex %= BUFFER_SIZE;
    writedIndex %= BUFFER_SIZE;
}

/*Devuelve la cantidad caracteres no leidos*/
int getFromStdBuffer(char *str, dword size){
    int i;
    _Sti();
    while(stdBufferIsEmpty());
    _Cli();
    for(i=0; i < size; i++) {
        if(stdBufferIsEmpty()){ 
           return size-i; 
        } else {
          *str=getCharFromBuffer();
          str++;
        }
    }
    return 0;
}

char getCharFromBuffer() {
        char c = buffer[toReadIndex];
        buffer[toReadIndex++] = 0;
        toReadIndex %= BUFFER_SIZE;
        return c;
}

void terminalPutHexa(int n) {
    int i;
    byte b;
    char str[10]={0};

    for(i=0; i<8; i++){
        b=n>>(28-i*4);
        b&=0x0F;
        if(b<10)
            str[i]='0'+b;
        else
            str[i]='A'+b-10;
    }
    str[i]='h';
    terminalPrint(str);
}


void terminalClearScreen(){
    videoClearScreen();
}

void terminalPrint(char *str) {
    while(*str)
        videoPutChar(*(str++));
}

void terminalPrintn(char* str, int size){
    int i;
    if(size==0){
        terminalPrint(str);
    }
    for(i=0; i<size; i++)
        videoPutChar(*(str++));
};