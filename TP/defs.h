#ifndef _defs_
#define _defs_

#define byte unsigned char
#define word short int
#define dword int

#define STD_INPUT_DESCRIPTOR 0
#define STD_OUTPUT_DESCRIPTOR 1

typedef struct {
	byte day;
	byte month;
	byte year;
} dateT;

typedef struct {
	byte second;
	byte minute;
	byte hour;
} timeT;

#endif