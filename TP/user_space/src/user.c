#include "../include/shell.h"
#include "../include/libc.h"

#define APPS_NUM	1

/*Inicializa el programa indicado por el indice*/
void initProgram(void (*apps[])(void), int idx);

int main() {
	int idx;
	void (*apps[])(void)={initShell};
	while(1) {
		printf("------- Main Menu ------\n");
		printf("Listado de aplicaciones:\n");
		printf("------------------------\n");
		printf("1 - Shell\n");
		printf("------------------------\n");
		do {
			idx=0;
			printf("Elija la aplicacion deseada");
			idx=getch()-'0';
			if(idx > APPS_NUM || idx <= 0){
				printf("\nParametro incorrecto.\nEscriba un numero que referencie a algun programa de la lista.\n");
			}
		} while(idx > APPS_NUM || idx <= 0);
		clearScreen();
		initProgram(apps, idx-1);
		clearScreen();
	}
	return 0;
}

void initProgram(void (*apps[])(void), int idx) {
	apps[idx]();
}