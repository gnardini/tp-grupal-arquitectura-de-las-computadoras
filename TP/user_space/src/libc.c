/************************************************/
/*                                              */
/*            Libreria para el user             */
/*                                              */
/************************************************/

#include "../include/libc.h"

void write(int fd, char *buffer, int size){
	_system_call(4,fd,buffer,size);
}

int read(int fd, char *buffer, int size) {
	int a=_system_call(3, fd, buffer, size);
	return a;
}

void clearScreen(){
	_system_call(5);
}

char getch(){
	return _system_call(7);
}

void print(char *str) {
	write(STD_OUTPUT_DESCRIPTOR,str,0);
}

void putChar(char c){
	write(STD_OUTPUT_DESCRIPTOR,&c,1);
}

char getNibble(byte b) {
	b &= 0x0F;
	if(b < 10)
		return '0' + b;
	else
		return 'A' + b - 10;
}

void putHexa(int n) {
	char str[8];
	int i;
	for(i=0; i < 8; i++) {
		str[i]=getNibble(n >> (28-4*i));
	}
	str[8]=0;
	write(STD_OUTPUT_DESCRIPTOR,str,8);
}

void putInt(int n) {
	char intString[DEF_INT_SIZE+1] = {0}; 
	int i;
	if(n<0){
		putChar('-');
		n*=-1;
	}
	if(n==0) {
		putChar('0');
	} else {
		for(i = DEF_INT_SIZE-1; i >= 0 && n > 0; i--, n/=10)
			intString[i]=n%10 + '0';
		print(intString+i+1); //Suma i para saltear los ceros
	}
}

void printf(char *fmt, ...) {
	int *args = (int *)(&fmt + sizeof(*fmt));
	while(*fmt) {
		switch(*fmt) {
			case '%':
				fmt++;
				switch(*fmt){
					case 'd':
						putInt(*((int *)args));
						args++;
						break;
					case 's':
						print((char *)*args);
						args++;
						break;
					case 'c':
						putChar(*args);
						args++;
						break;
					case 'x':
						putHexa(*args);
						args++;
						break;
					default:
						putChar('%');
						putChar(*fmt);
						break;
				}
				break;
			case '\\':
				putChar(*fmt);
				fmt++;
				if(!*fmt){
					fmt--;
					break; //Por si es un 0	
				} 
			default:
				putChar(*fmt);
		}
		fmt++;
	}
}

char getChar() {
	char c = 0;
	read(STD_INPUT_DESCRIPTOR,&c,1);
	return c;
}


int scanf(const char *fmt, ...) {
	int *args = (int *)(&fmt + sizeof(*fmt));
	int argsRead = 0;
	char c;
	int n;
	while(*fmt) {
		switch(*fmt) {
			case '%':
				fmt++;
				switch(*fmt) {
					case 'd':
						c = getInt((int*)*args);
						if(c==-1 || c=='\n')
							return argsRead;
						argsRead++;
						args++;
						if(c != *(++fmt)) {
							return argsRead;
						}
						break;
					case 's':
						n = read(STD_INPUT_DESCRIPTOR,(char*)*args,512);
						*(((char*)*args)+(511-n))='\0';	//Reemplaza el \n por \0
						args++;
						return argsRead+1;
					case 'c':
						c = getChar();
						*((char *)*args) = c;
						args++;
						argsRead++;
						break;
					default:
						c = getChar();
						if(c!='%')
							return argsRead;
						c = getChar();
						if(*fmt!=c)
							return argsRead;					
				}
				break;
			case '\\':
				if(*(++fmt)=='%') {
					c = getChar();
					if('%'!= c)
						return argsRead;
					break;
				}
				c = getChar();
				if('\\' != c)
					return argsRead;
			default:
				c=getChar();
				if(*fmt != c) {
					return argsRead;
				}

		}
		fmt++;
	}	
	return argsRead;
}


/*Devuelve -1 si no pudo crear el numero, o el caracter que corto numero*/
char getInt(int *num) {
	char c = getChar();
	int signFlag = 1;
	int  n = 0;

	if(c == '-') {
		signFlag = -1;
		c = getChar();
	}
	if(c < '0' || c > '9')
		return -1;
	while(c >= '0' && c <= '9') {
		n = n*10+ (c -'0');
		c = getChar();
	}
	*num=n*signFlag;
	return c;
}

/*str1 siempre se lee del buffer, str2 es un string comun*/
int strcmp(char *str1, char *str2) {
	while((*str1 != '\0') && (*str2 != '\0') && (*str1 == *str2)) {
		str1++;
		str2++;
	}
	return *str1 == *str2;
}


void infoIDT(){
	int i=0;
	infoIDTTitle();
	while(_system_call(0x06)){
		printf("\n");
		if(++i%20==0){
			printf("\nPresione una tecla para continuar");
			getch();
			infoIDTTitle();
		}
	}
	printf("\n");
}

void infoIDTTitle(){
	clearScreen();
	printf("Mostrando informacion de la IDT\n\n");
}

void printBiosInfo() {
	_system_call(8);
}

void getDate(dateT *date){
	_system_call(9, date);
}

void getTime(timeT *curr_time) {
	_system_call(10, curr_time);
}

void setExceptionSound(int exception, word frequency, int duration){
	_system_call(11, exception, frequency, duration);
}

void makeSound(word frequency, int duration){
	_system_call(12, frequency, duration);
}

int clearBuffer(){
	return _system_call(13);
}
