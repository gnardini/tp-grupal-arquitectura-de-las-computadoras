#include "../include/shell.h"
#include "../include/libc.h"

char cmdBuffer[CMD_BUFFER_SIZE] = {0};

int interrupted = 0;

void initShell() {
	tCommand programs[PROGRAMS_NUM]; /*Todos los comandos disponibles*/
	char *cmdPrograms[] = {"help","bios","exit","clear","idt", "zero", "general", "time", "date", "setsound", "testsound", "overflow"};
	void (*fpPrograms[])() = {&execHelp, &execBios, &execExit, &clearScreen, &infoIDT, &_zero, &_general_protection, &showTime, &showDate, &setSound, &soundTest, &_overflow};
	int program;

	interrupted = 0;
	loadPrograms(cmdPrograms, fpPrograms, programs);
	initMenu();
	while(!interrupted) {
		printf("shell@kernel> ");
		program = scanCmd(programs);
		if(program == UNDEFINED_PROGRAM)
			printf("Instruccion no soportada.\nPresione \"help\" para mas informacion.\n");
		else
			executeProgram(programs, program);
	}
	return;
}

void initMenu() {
	printf("--- Bienvenido al Shell ---\n\n");
}

void loadPrograms(char *cmdPrograms[], void (*fpPrograms[])(),tCommand programs[]) {
	
	
	int i;
	for(i=0; i<PROGRAMS_NUM; i++) {
		programs[i].cmd = cmdPrograms[i]; 
		programs[i].fp = fpPrograms[i];
	}
}

int scanCmd(tCommand programs[]) {
	scanf("%s", cmdBuffer);
	return parseCmd(cmdBuffer, programs);
}

int parseCmd(char *cmd, tCommand programs[]) {
	int i;

	for(i=0; i < PROGRAMS_NUM; i++) {
			//char *aux = programs[i]->cmd;
			if(strcmp(cmd, programs[i].cmd)) {
				return i;
		}
	}
	return UNDEFINED_PROGRAM;
}

void executeProgram(tCommand programs[], int idx) {
	(programs[idx].fp)();
	clearBuffer();
}

void execHelp() {
	printf("\nComandos admitidos:\n\n");
	printf("bios:       Devuelve la marca, la version y la fecha de fabricacion del bios\n");  
	printf("exit:       Termina la ejecucion del Shell\n");
	printf("clear:      Limpia la pantalla\n");
	printf("idt:        Devuelve cada entrada de la IDT, con su respectivo selector, Offset y nivel de acceso\n");
	printf("zero:       Emula la excepcion de division por zero(0)\n");
	printf("overflow:   Emula la excepcion de overflow(4)\n");
	printf("general:    Emula la excepcion de proteccion general(13)\n");
	printf("time:       Devuelve la hora actual del sistema\n");
	printf("date:       Devuelve la fecha actual del sistema\n");
	printf("setsound:   Permite personalizar los sonidos para cada excepcion\n");
	printf("testsound:  Muestra una prueba de sonido\n\n");
	
	
}

void execExit() {
	printf("Exiting shell... Good bye!\n");
	interrupted = 1; //Sale del while y vuelve a MainMenu
}

void execBios() {
	printBiosInfo();
	putChar('\n');
}

void showTime(){
	timeT curr_time;
	
	getTime(&curr_time);
	printf("Hora: %d:%d:%d\n", curr_time.hour, curr_time.minute, curr_time.second);
}

void showDate(){
	dateT date;
	
	getDate(&date);
	printf("Fecha: %d/%d/%d\n", date.day, date.month, 2000+date.year);
}

void setSound(){
	int exception, frequency, duration;

	printf("Introduzca el numero de excepcion para el cual quiere cambiar el sonido: ");
	scanf("%d", &exception);
	if(!clearBuffer() || exception > 32 || exception < 0 ){
		printf("Parametros invalidos\n");
		return;
	}

	printf("Introduzca la frecuencia deseada para el sonido de la excepcion %d: ", exception);
	scanf("%d", &frequency);
	if(!clearBuffer() || frequency <= 0){
		printf("Parametros invalidos\n");
		return;
	}

	printf("Introduzca la duracion en milisegundos del sonido: ");
	scanf("%d", &duration);
	if(!clearBuffer() || duration <= 0){
		printf("Parametros invalidos\n");
		return;
	}
	putChar('\n');
	
	setExceptionSound(exception, (word)frequency, duration);
}

void soundTest(){
	int frec;
	for(frec=100; frec <= 1400; frec+=100)
		makeSound(frec,350);
	for(frec=1400; frec >= 100; frec-=100)
		makeSound(frec,250);
}