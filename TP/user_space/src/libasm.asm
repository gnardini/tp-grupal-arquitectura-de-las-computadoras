GLOBAL _system_call, _zero, _general_protection, _overflow

EXTERN putHexa

SECTION .text

_general_protection:
	jmp -1
	retn

_zero:
	push	ebp
	mov		ebp, esp
	mov	 	eax, 0
	div 	eax
	retn

_overflow:
	mov al, 127
	add al, 127
	into
	retn

_system_call:
	push ebp
	mov ebp, esp
	push ebx
	push ecx
	push edx
	push esi
	push edi

	mov eax, [ebp+8]
	mov ebx, [ebp+12]
	mov ecx, [ebp+16]
	mov edx, [ebp+20]
	mov esi, [ebp+24]
	mov edi, [ebp+28]
	int 80h

	pop edi
	pop esi
	pop edx
    pop ecx
    pop ebx
    mov esp, ebp
    pop ebp
    ret