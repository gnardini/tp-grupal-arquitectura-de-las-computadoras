#ifndef _libasm_
#define _libasm_

#include "../../defs.h"

void _general_protection();

void _zero();

void _bounds();

void _overflow();

dword _system_call(int eax, ...);

#endif
