#ifndef _shell_
#define _shell_

#include "./libasm.h"

#define CMD_BUFFER_SIZE 512
#define PROGRAMS_NUM 12
#define UNDEFINED_PROGRAM -1

typedef struct {
	void (*fp)(void);
	char *cmd;
} tCommand;

void initShell();

void initMenu();

void loadPrograms(char *cmdPrograms[], void (*fpPrograms[])(),tCommand programs[]);

int scanCmd(tCommand programs[]);

int parseCmd(char *cmd, tCommand programs[]);

void executeProgram(tCommand programs[], int idx);

void execHelp();

void execExit();

void execBios();

void showTime();

void showDate();

void setSound();

void soundTest();

#endif