#ifndef _lib_c_
#define _lib_c_

#include "./libasm.h"
#include "../../defs.h"

#define DEF_INT_SIZE 11

int read(int fd, char *buffer, int size);

void write(int fd, char *buffer, int size);

void clearScreen();

void putChar(char c);

char getch();

char getNibble(byte b);

void putHexa(int n);

void putInt(int n);

void print(char *str);

void printf(char *fmt, ...);

int clearBuffer();

char getChar();

char getInt(int *num);

int scanf(const char *fmt, ...);

int readInput(char *buffer, int size);

int strcmp(char *str1, char *str2);

void infoIDT();

void infoIDTTitle();

void printBiosInfo();

void getDate(dateT *date);

void getTime(timeT *curr_time);

void setExceptionSound(int exception, word frequency, int duration);

void makeSound(word frequency, int duration);

#endif