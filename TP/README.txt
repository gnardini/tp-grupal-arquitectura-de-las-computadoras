/\ \/\ \                              /\_ \     
\ \ \/'/'     __   _ __    ___      __\//\ \    
 \ \ , <    /'__`\/\`'__\/' _ `\  /'__`\\ \ \   
  \ \ \\`\ /\  __/\ \ \/ /\ \/\ \/\  __/ \_\ \_ 
   \ \_\ \_\ \____\\ \_\ \ \_\ \_\ \____\/\____\
    \/_/\/_/\/____/ \/_/  \/_/\/_/\/____/\/____/
                                                
Integrantes:
- Vazquez, Diego
- Ibars Ingman, Gonzalo
- Nardini, Gonzalo

Estructura de archivos:
-----------------------
Se decidió dividir en 2 carpetas los espacios de front end y back end del kernel (respectivamente, user_space y kernel_space) para realizar una que sea más evidente la separación entre los mismos.
Dentro de cada space se crearon dos carpetas:
- src:			Contiene el codigo de todos los archivos
- include:		Contiene los archivos headers
El user_space incluye una librería para comunicarse con la API del kernel y una interfaz con un menu de aplicaciones.
El kernel_space comprende de todo lo relacionado entre la comunicación de hardware y el software, incluyendo las interrupciones y drivers que se comunican con los perifericos o chips.
Además, se posee el archivo "defs.h" que contiene los tipos de dato que se comparten en ambos espacios.

El user_space contiene:
- user.c:		Representa la interfaz principal del usuario. Posee una lista con los programas a utilizar.
- shell.c:		Es el programa principal del user.c. Contiene una lista de comandos para ser ejecutados por el usuario con el objetivo de demostrar el funcionamiento del Kernel.
- libc.c:		Contiene la API(C) que comunica al usuario con las funciones del Kernel.
- libasm.asm:		Contiene la API(Assembly) que comunica al usuario con las funciones del Kernel.

El kernel_space contiene:
- loader.asm:		Contiene las funciones y parámetros iniciales para elevar el kernel. Desde acá se llama al kernel.c y luego al user.c
- kernel.c:		Inicializa la pantalla, setea las frecuencias de sonido para las excepciones, carga las entradas de la IDT, setea las máscaras del PIC.
- terminal.c:		Es un complemento del Kernel que se comunica con el STD Input y Output.
- interruptions.c:	Es un complemento del Kernel que contiene las rutinas de atención de las interrupciones y toda la informacion de la IDT.
- kasm.asm:		Es una librería que contiene los handlers de las interrupciones y excepciones. A su vez, contiene la rutina para cargar la IDTR, para inhibir y permitir interrupciones entrantes (Excepto las NMI) y para leer o escribir datos por la E/S.
- kc.c:			Contiene funciones auxiliares para realizar las tareas del Kernel.

En la carpeta drivers se encuentran:
- video_driver.c:	Implementa las funciones que imprimen en pantalla.
- sound_driver.c:	Implementa funciones que reproducen un sonido con el Speaker de la PC. Parametrizado por frecuencia y tiempo(ms).
- pit_driver.c:		Implementa funciones para utilizar el timer tick en conjunto con algún otro dispositivo.
- pic_driver.c: 	Implementa funciones para remapear el las interrupciones del pic en la idt, y además para establecer las mascaras del pic. 
- cmos_driver.c:	Implementa funciones para obtener la fecha y hora actual del RTC, contenido dentro del mismo espacio que el CMOS.
- bios_driver.c:	Implementa una función para obtener la marca, versión y fecha de fabricación del BIOS(Basic Input/Output System).

Consideraciones:
----------------
Algunos códigos fueron extraidos de paginas web, o algunos trabajos practicos de cuatrimestres anteriores, y estos fueron modificados para poder utilizarlos en nuestra implementación.
- La matriz de teclas que se encuentra en keyboard_drive.c fue extraída de un trabajo practico del cuatrimestre anterior (bitbucket.org/mishuagopian/arquitectura-tpe).
- El código de la función changePicOffset en pic_driver.c fue escrito basado en wiki.osdev.org/PIC#Initialisation y se uso como información extra la página stanislavs.org/helppc/8259.html
- El código de información para la realización de la funcion bios_routine en bios_driver.c fue extraído de la página wiki.osdev.org/System_Management_BIOS.
- La información para la realización del cmos_driver.c fue extraído de la página wiki.osdev.org/CMOS.
- El código de información para la realización de sound_driver.c fue extraída de wiki.osdev.org/PC_Speaker



Decisiones y justificaciones:
-----------------------------
- Se decidió que el kernel space sea el encargado de imprimir por pantalla cada vez que se presione una tecla a través de la terminal en vez de que sea el user space quien tome la responsabilidad. La ventaja que trae aparejada esta decisión es los codigos en el user space se independizan de la impresion por pantalla tal como ocurre en los programas actuales, ademas de evitarse estar todo el tiempo consultando el buffer del teclado. Sin embargo, la desventaja que trae esta decisión es que el usuario no posee la lógica de impresión, con lo cual se podrían traer aparejado problemas como por ejemplo borrar datos sensibles.
- Se escogió mantener un buffer de la entrada estandar del lado del kernel space en vez del user space. Esta decisión se relaciona directamente con la anterior ya que no tendría sentido que este buffer esté en el user siendo el kernel quien imprime porque el user tendría que estar consultando todo el tiempo si se presionó una tecla.
- Para el caso del getChar y scanf, se tomó la decisión de que mientras esté vacío el buffer, el que queda esperando una respuesta del teclado es el Kernel, es decir, el usuario pierde el control del programa hasta que el Kernel recibe una respuesta y le devuelve el control. Una ventaja de esta decisión, es que se evita repetitivos llamados del usuario al kernel (preguntando si el buffer está vacío), evitando así realizar menos system calls. En los programas convencionales ésto es lo que ocurre, ya que cuando se llama a la función getChar, se está esperando una entrada por parte de quien administra los recursos del sistema, lo cual produce un código más limpio.
- Se decidió crear una funcion auxiliar "wait" para emular una pausa en el sistema, utilizando el timer tick como dispositivo de medida. Cabe aclarar que esta función no es exacta, sino que detiene la ejecución en intervalos múltiplos de 55 milisegundos. Esta función resulta útil para poder decidir la longitud del pitido del Speaker de forma más exacta, comparado con utilizar un ciclo para representar la cantidad de tiempo en el cual se reproduce el pitido, dado que distintos procesadores, tendrían distintos tiempos para el mismo ciclo.
- Se tomó la decisión de que la información sobre el BIOS y las entradas de la IDT se impriman en el kernel para no romper con el encapsulamiento de darle información al usuario que no debería poder manipular. Se intentó resolver el problema por otro lado, pero a pesar de los intentos por realizar una alocación dinámica de memoria para poder obtener una copia profunda de la información (y por lo tanto segura), éstos siempre traían aparejados otros problemas como el "pisado" de memoria.
- Para el caso de las excepciones, se optó por utilizar la información del Stack para retornar a la dirección de memoria de la dirección de retorno de la función que generó la excepción. De esta forma, el programa puede intenta continuar su normal ejecución. La gran desventaja que trae esta decisión es que el contexto de la función que llamó a la función que genera la excepción puede ser modificado de manera imprevista (ya que la función que genera la excepción no termina de manera habitual y puede omitirse la recuperación del contexto anterior) y puede llevar a errores fatales. Sin embargo, consideramos preferible esta opción antes de colgar el sistema directamente o reiniciarlo.


Instrucciones de Uso:
---------------------
- Si se desea compilar el trabajo, ejecutar el archivo "compila"
- Si se desea ejecutar el kernel.bin desde el qemu, previamente habiendo configurado el disco virtual desde .mtoolsrc, se debe ejecutar el archivo "ejecutar"
- Si se desea obtener una copia con formato .iso del kernel, ejecutar "toISO"
